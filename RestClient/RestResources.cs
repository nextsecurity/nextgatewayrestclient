﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClient
{
    public class RestResources
    {
        public const string StatusPath = "nextgateway/status";
        public const string CreateFilePath = "tasks/create/file";
        public const string CreateUrlPath = "tasks/create/url";
        public const string TasksListPath = "tasks/list";
        public const string ReportPath = "tasks/report/{0}/{1}"; // 0: ID, 1: Format - json/html/maec/metadata/all/dropped
        public const string MachinesListPath = "machines/list";        
    }
}
