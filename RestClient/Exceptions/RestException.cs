﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClient.Exceptions
{
    public class RestException : Exception
    {
        public IRestResponse RestResponse { get; private set; }

        public RestException(IRestResponse restResponse)
        {
            RestResponse = restResponse;
        }
    }
}
