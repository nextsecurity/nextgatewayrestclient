﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClient.Exceptions
{
    public class FileDownloadException : Exception
    {
        public FileDownloadException(Exception exception) : base(exception.Message, exception)
        {
        }
    }
}
