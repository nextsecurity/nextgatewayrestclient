﻿using Newtonsoft.Json;
using NextSecurity.NextGateway.RestClient.Exceptions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClient
{
    public class Client : IRestClient
    {
        #region Privates

        private readonly RestSharp.RestClient restClient;

        #endregion Privates

        #region Properties

        public IEnumerable<dynamic> Tasks { get { return getTasks(); } }
        public dynamic Status { get { return getStatus(); } }
        public IEnumerable<dynamic> Machines { get { return getMachines(); } }

        #endregion Properties

        #region Constructors

        public Client(string ApiAddress)
        {
            restClient = new RestSharp.RestClient();
            restClient.BaseUrl = ApiAddress;
        }

        #endregion Constructors

        #region PublicMethods

        public uint CreateFileTask(string filePath, uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false)
        {
            return createTask((parameters) =>
            {
                return request(RestResources.CreateFilePath, Method.POST, parameters, filePath);
            },
            timeout, package, options, enforce_timeout);
        }

        public uint CreateUrlTask(string url, uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false)
        {
            return createTask((parameters) =>
            {
                parameters.Add(new Parameter { Type = ParameterType.GetOrPost, Name = "url", Value = url });

                return request(RestResources.CreateFilePath, Method.POST, parameters);
            },
            timeout, package, options, enforce_timeout);
        }

        public void GetReport(uint reportId, string format, string filePath)
        {
            Uri address = null;

            if (Uri.TryCreate(new Uri(restClient.BaseUrl), string.Format(RestResources.ReportPath, reportId, format), out address))
            {
                using (var client = new WebClient())
                {
                    client.DownloadFile(address.ToString(), filePath);
                }
            }
        }

        #endregion PublicMethods

        #region PrivateMethods

        private IEnumerable<dynamic> getTasks()
        {
            var tasks = new List<dynamic>();
            dynamic jsonTasks = request(RestResources.TasksListPath);
            foreach (dynamic task in jsonTasks.tasks)
            {
                tasks.Add(task);
            }

            return tasks;
        }

        private IEnumerable<dynamic> getMachines()
        {
            var machines = new List<dynamic>();
            dynamic jsonMachines = request(RestResources.MachinesListPath);
            foreach (dynamic machine in jsonMachines.machines)
            {
                machines.Add(machine);
            }

            return machines;
        }

        private dynamic getStatus()
        {
            return request(RestResources.StatusPath);
        }

        private uint createTask(Func<List<Parameter>, dynamic> func, uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false)
        {
            List<Parameter> parameters = createParameters(timeout, package, options, enforce_timeout);
            dynamic result = func.Invoke(parameters);

            if (result.task_id == null)
            {
                throw new Exception("Error, task not created!");
            }

            return result.task_id;
        }

        private List<Parameter> createParameters(uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false)
        {
            var parameters = new List<Parameter>();

            if (timeout.HasValue)
            {
                parameters.Add(new Parameter { Type = ParameterType.GetOrPost, Name = "timeout", Value = timeout.Value });
            }
            if (package != null)
            {
                parameters.Add(new Parameter { Type = ParameterType.GetOrPost, Name = "package", Value = package });
            }
            if (options != null)
            {
                parameters.Add(new Parameter { Type = ParameterType.GetOrPost, Name = "options", Value = options });
            }
            if (enforce_timeout)
            {
                parameters.Add(new Parameter { Type = ParameterType.GetOrPost, Name = "enforce_timeout", Value = enforce_timeout });
            }

            return parameters;
        }

        private dynamic request(string resource, Method method = Method.GET, List<Parameter> parameters = null, string filepath = null)
        {
            IRestResponse restResponse;

            return request(resource, out restResponse, method, parameters, filepath);
        }

        private dynamic request(string resource, out IRestResponse outRestResponse, Method method = Method.GET, List<Parameter> parameters = null, string filepath = null)
        {
            var request = new RestRequest
            {
                Resource = resource,
                Method = method,
            };


            if (filepath != null)
            {
                //using (var fileStream = new FileStream(filepath, FileMode.Open))
                //{
                //request.Files.Add(new FileParameter
                //{
                //    FileName = Path.GetFileName(filepath),
                //    Name = "file",
                //    Writer = s =>
                //    {
                //        fileStream.CopyTo(s);
                //    },
                //});
                //request.AddBody(new { 
                //request.AddParameter("file", Path.GetFileName(filepath)); 
                //request.AddFile("file", System.IO.File.ReadAllBytes(filepath), Path.GetFileName(filepath), "application/octet-stream");
                request.AddFile("file", filepath);
                //}
            }

            //request.AddHeader("Content-Type", "multipart/form-data");
            request.RequestFormat = DataFormat.Json;
            if (parameters != null)
            {
                foreach (Parameter parameter in parameters)
                {
                    request.AddParameter(parameter);
                }
            }

            outRestResponse = restClient.Execute(request);
            if (outRestResponse.ResponseStatus != ResponseStatus.Completed)
            {
                throw new RestException(outRestResponse);
            }

            return JsonConvert.DeserializeObject<dynamic>(outRestResponse.Content);
        }

        #endregion PrivateMethods
    }
}
