﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClient
{
    public interface IRestClient
    {
        IEnumerable<dynamic> Tasks { get; }

        dynamic Status { get; }

        IEnumerable<dynamic> Machines { get; }

        uint CreateFileTask(string filePath, uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false);

        uint CreateUrlTask(string url, uint? timeout = null, string package = null, string options = null, bool enforce_timeout = false);

        void GetReport(uint reportId, string format, string filePath);
    }
}
