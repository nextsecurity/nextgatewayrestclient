﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NextSecurity.NextGateway.RestClientTest
{
    [TestClass]
    public class MainTests
    {
        private const string ApiAddress = "http://www.nextgateway.com:8090";

        [TestMethod]
        public void TestStatus()
        {
            var restClient = new RestClient.Client(ApiAddress);
            dynamic status = restClient.Status;
            string version = status.version;
            Assert.AreEqual(version, "1.2-dev");
        }

        [TestMethod]
        public void TestTasks()
        {
            var restClient = new RestClient.Client(ApiAddress);
            IEnumerable<dynamic> tasks = restClient.Tasks;
            Assert.IsFalse(tasks.Count() == 0);
        }

        [TestMethod]
        public void TestMachines()
        {
            var restClient = new RestClient.Client(ApiAddress);
            IEnumerable<dynamic> machines = restClient.Machines;
            Assert.IsFalse(machines.Count() == 0);
        }

        [TestMethod]
        public void TestReport()
        {
            string filepath = @"score.json";
            var restClient = new RestClient.Client(ApiAddress);
            restClient.GetReport(1, "score", filepath);
            Assert.IsTrue(File.Exists(filepath));
            File.Delete(filepath);
        }

        [TestMethod]
        public void TestNewUrlTask()
        {
            var restClient = new RestClient.Client(ApiAddress);
            uint taskId = restClient.CreateUrlTask("http://www.ynet.co.il", 90);
            Assert.IsTrue(taskId != 0);
        }

        [TestMethod]
        public void TestNewFileTask()
        {
            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "calc.exe");
            var restClient = new RestClient.Client(ApiAddress);
            uint taskId = restClient.CreateFileTask(filePath, 90);
            Assert.IsTrue(taskId != 0);
        }

        //[TestMethod]
        //public void TestNewFile2Task()
        //{
        //    string filePath = @"C:\Users\Dev\Desktop\7tt_setup.exe";
        //    var restClient = new RestClient.Client(ApiAddress);
        //    uint taskId = restClient.CreateFileTask(filePath, 90);
        //    Assert.IsTrue(taskId != 0);
        //}
    }
}